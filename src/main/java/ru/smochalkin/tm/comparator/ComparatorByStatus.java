package ru.smochalkin.tm.comparator;

import ru.smochalkin.tm.api.model.IHasStatus;
import java.util.Comparator;

public class ComparatorByStatus implements Comparator<IHasStatus> {

    public final static ComparatorByStatus INSTANCE = new ComparatorByStatus();

    private ComparatorByStatus(){}

    @Override
    public int compare(IHasStatus o1, IHasStatus o2) {
        if (o1.getStatus() == null) return 1;
        if (o2.getStatus() == null) return -1;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}