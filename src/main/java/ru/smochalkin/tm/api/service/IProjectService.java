package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

}
