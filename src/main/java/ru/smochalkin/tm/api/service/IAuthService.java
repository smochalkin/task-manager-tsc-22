package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.User;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    User getUser();

    void checkRoles(Role... roles);

}
