package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.User;

public interface IUserService extends IService<User> {

    User findByLogin(String login);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateById(String userId, String firstName, String lastName, String middleName);

    boolean isLogin(String login);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
