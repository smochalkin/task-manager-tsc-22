package ru.smochalkin.tm.command.system;

import ru.smochalkin.tm.command.AbstractSystemCommand;

public final class VersionCommand extends AbstractSystemCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Display version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.10.0");
    }

}
