package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "Lock a user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
