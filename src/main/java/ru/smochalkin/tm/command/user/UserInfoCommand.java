package ru.smochalkin.tm.command.user;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.model.User;

public class UserInfoCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-info";
    }

    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("User id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
