package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String description() {
        return "Start task by id.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().getUserId();
        System.out.print("Enter id: ");
        String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        serviceLocator.getTaskService().updateStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
