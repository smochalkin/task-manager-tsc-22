package ru.smochalkin.tm.command.task;

import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String description() {
        return "Start task by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findByName(userId, name);
        serviceLocator.getTaskService().updateStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
