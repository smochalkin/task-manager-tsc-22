package ru.smochalkin.tm.command;

import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    public Role[] roles(){
        return null;
    }

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String description = description();
        if(name != null && !name.isEmpty()) result += name + " ";
        if(description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
