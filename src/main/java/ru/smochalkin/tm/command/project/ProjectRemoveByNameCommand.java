package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.entity.ProjectNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter project name: ");
        String projectName = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectTaskService().isProjectName(userId, projectName)) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectTaskService().removeProjectByName(userId, projectName);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
