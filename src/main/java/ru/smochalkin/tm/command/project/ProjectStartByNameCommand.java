package ru.smochalkin.tm.command.project;

import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(userId, name);
        serviceLocator.getProjectService().updateStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
