package ru.smochalkin.tm.model;

import ru.smochalkin.tm.api.model.IWBS;
import ru.smochalkin.tm.enumerated.Status;

import java.util.Date;

public class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    protected String name;

    protected String description;

    protected Status status = Status.NOT_STARTED;

    protected Date created = new Date();

    protected Date startDate;

    protected Date endDate;

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AbstractBusinessEntity() {
    }

    public AbstractBusinessEntity(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name + ": " + description +
                "; Status - " + status.getDisplayName() +
                "; Created - " + created +
                "; Start - " + startDate +
                "; End - " + endDate +
                "; userId - " + userId;
    }

}
